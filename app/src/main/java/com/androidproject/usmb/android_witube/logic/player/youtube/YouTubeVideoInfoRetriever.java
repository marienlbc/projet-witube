package com.androidproject.usmb.android_witube.logic.player.youtube;

import android.support.v4.app.Fragment;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;
import java.util.TreeMap;

/**
 * Retrieve youtube video information
 */
public class YouTubeVideoInfoRetriever{

    private static final String URL_YOUTUBE_GET_VIDEO_INFO = "http://www.youtube.com/get_video_info?&video_id=";
    private static String targetUrl = "";

    private static TreeMap<String, String> kvpList = new TreeMap<>();

    private IYoutubeVideoInfoRetrieverCallback mIYoutubeVideoInfoRetrieverCallback;

    public YouTubeVideoInfoRetriever(Fragment fragment, String videoId){
        targetUrl = URL_YOUTUBE_GET_VIDEO_INFO + videoId;
        if(!videoId.equals("")) new SimpleHttpClient().execute();
        else {
            mIYoutubeVideoInfoRetrieverCallback = (IYoutubeVideoInfoRetrieverCallback) fragment;
            mIYoutubeVideoInfoRetrieverCallback.retrieveYoutubeVideoThumbnail("");
        }
    }

    public void setRetriever(IYoutubeVideoInfoRetrieverCallback iYoutubeVideoInfoRetrieverCallback) {
        this.mIYoutubeVideoInfoRetrieverCallback = iYoutubeVideoInfoRetrieverCallback;
    }

    private String getInfo(String key) {
        return (kvpList.get(key) != null) ? kvpList.get(key) : "";
    }

    /**
     * Debug
     */
    @SuppressWarnings("unused")
    public void printAll() {
        System.out.println("TOTAL VARIABLES=" + kvpList.size());

        for(Map.Entry<String, String> entry : kvpList.entrySet()) {
            System.out.print( "" + entry.getKey() + "=");
            System.out.println("" + entry.getValue() + "");
        }
    }

    private static void parse(String data) throws UnsupportedEncodingException {
        String[] splits = data.split("&");
        String kvpStr;

        if(splits.length < 1)
        {
            Log.d("parse", "splits.length < 1");
            return;
        }

        kvpList.clear();

        for (String split : splits) {
            kvpStr = split;

            try {
                // Data is encoded multiple times
                kvpStr = URLDecoder.decode(kvpStr, SimpleHttpClient.ENCODING_UTF_8);
                kvpStr = URLDecoder.decode(kvpStr, SimpleHttpClient.ENCODING_UTF_8);

                String[] kvpSplits = kvpStr.split("=", 2);

                if (kvpSplits.length == 2) {
                    kvpList.put(kvpSplits[0], kvpSplits[1]);
                    Log.d("parse", "kvpSplits[0], kvpSplits[1] " + kvpSplits[0] + " , " + kvpSplits[1]);
                } else if (kvpSplits.length == 1) {
                    kvpList.put(kvpSplits[0], "");
                    Log.d("parse", "kvpSplits[0], \"\" " + kvpSplits[0]);
                }
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
                throw ex;
            }
        }
    }

    private class SimpleHttpClient extends AsyncTask<Void, Void, String> {
        private static final String ENCODING_UTF_8 = "UTF-8";
        private static final int DEFAULT_TIMEOUT = 10000;

        private static final String HTTP_GET = "GET";

        private String getInput(InputStream in) throws IOException {
            StringBuilder sb = new StringBuilder(8192);
            byte[] b = new byte[1024];
            int bytesRead;

            while (true) {
                bytesRead = in.read(b);
                if (bytesRead < 0) {
                    break;
                }
                String s = new String(b, 0, bytesRead, ENCODING_UTF_8);
                sb.append(s);
            }

            return sb.toString();
        }

        @Override
        protected String doInBackground(Void... params) {
            HttpURLConnection conn = null;
            String response = "";

            try {
                URL url = new URL(targetUrl);
                conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(SimpleHttpClient.DEFAULT_TIMEOUT);
                conn.setRequestMethod(SimpleHttpClient.HTTP_GET);
                conn.connect();
                if(conn.getErrorStream() == null){
                    InputStream inStream = new BufferedInputStream(conn.getInputStream());
                    response = getInput(inStream);
                }
                else{
                    String errorResponse = " : ";
                    errorResponse = errorResponse + getInput(conn.getErrorStream());
                    response = response + errorResponse;
                }
            } catch (IOException e) {
                e.printStackTrace();
                if(e.getMessage() != null) Log.e("SimpleHttpClient", e.getMessage());
            } finally {
                if(conn != null){
                    conn.disconnect();
                }
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                parse(s);
                Log.i("1 - thumb", getInfo("thumbnail_url"));
                mIYoutubeVideoInfoRetrieverCallback.retrieveYoutubeVideoThumbnail(getInfo("thumbnail_url"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                if(e.getMessage() != null) Log.e("SimpleHttpClient", e.getMessage());
            }
            //Log.i("json", s);
        }
    }
}