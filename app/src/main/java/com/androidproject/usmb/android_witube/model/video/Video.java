package com.androidproject.usmb.android_witube.model.video;

import java.util.ArrayList;

public class Video {

    private String id;
    private String title;
    private String user_id;
    private String url;
    private String date;
    private String comment;
    private ArrayList<String> tags = new ArrayList<>();
    private String videoThumbnail; //TODO see what type to put
    private String provider;
    /* for WiDevine Technology, could be implemented later on: see http://www.widevine.com/ */
    private String contentId;
    private String type;
    private String source;

    public Video() {
    }

    public Video(String id, String title, String url, String date, String comment, ArrayList<String> tags) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.date = date;
        this.comment = comment;
        this.tags = tags;
    }

    public Video(String id, String title, String url, String date, String comment,
                 ArrayList<String> tags, String videoThumbnail, String type, String source) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.date = date;
        this.comment = comment;
        this.tags = tags;
        this.videoThumbnail = videoThumbnail;
        this.type = type;
        this.source = source;
    }

    public Video(String id, String title, String url, String date, String comment,
                 ArrayList<String> tags, String videoThumbnail, String contentId, String provider, String type, String source) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.date = date;
        this.comment = comment;
        this.tags = tags;
        this.videoThumbnail = videoThumbnail;
        this.contentId = contentId;
        this.provider = provider;
        this.type = type;
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {return user_id;
    }

    public void setUser_id(String user_id) {this.user_id = user_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
