package com.androidproject.usmb.android_witube.utilities.volley;

import com.android.volley.Cache;
import com.android.volley.VolleyLog;
import com.androidproject.usmb.android_witube.AppController;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 *  Volley Helper Class
 */
public abstract class VolleyHelper {

    public static final String NO_CACHE_DATA = "no-cache-data";
    public static final String CACHE_ERROR = "Data Cache Error";
    public static final String CACHE_EXPIRED = "Cache Expired";
    private static final String TAG = "VolleyHelper"; //for debug

    /**
     * get response from cache
     * handle automatically if the screen is rotated
     * @param data_url : key value to retrieve data from cache
     * @return String
     */
    public static String getDataFromCache(String data_url, boolean non_expired_flag){
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(data_url);

        if (entry != null){
            if(non_expired_flag && entry.isExpired()){
                return CACHE_EXPIRED;
            } else {
                try {
                    System.out.println(" ---  cache : " + Arrays.toString(entry.data));
                    return new String(entry.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.d(TAG, "Cache Error: " + e.getMessage());
                    e.printStackTrace();
                    return CACHE_ERROR;
                }
            }
        } else{
            return NO_CACHE_DATA;
        }

    }

}
