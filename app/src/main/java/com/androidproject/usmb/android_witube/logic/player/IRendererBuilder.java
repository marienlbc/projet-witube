package com.androidproject.usmb.android_witube.logic.player;

import com.androidproject.usmb.android_witube.activities.menu_fragments.PlayerView;

public interface IRendererBuilder {
        void buildRenderer(PlayerView player);

        @SuppressWarnings("unused")
        void cancel();
}
