package com.androidproject.usmb.android_witube.utilities.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferenceHelper {

    private static final String PREF_KEY_USER_SESSION = "userSession";

    @SuppressWarnings("WeakerAccess")
    public static SharedPreferences getSharedPreferences(Context ctx) { return PreferenceManager.getDefaultSharedPreferences(ctx); }

    public static void setUserSession(Context ctx, String user) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_KEY_USER_SESSION, user);
        editor.apply();
    }

    public static String getUserSession(Context ctx) { return getSharedPreferences(ctx).getString(PREF_KEY_USER_SESSION, ""); }

    public static void clearUserSession(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove(PREF_KEY_USER_SESSION); //clear all stored data
        editor.apply();
    }

}
