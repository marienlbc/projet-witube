package com.androidproject.usmb.android_witube.logic.video;

import com.androidproject.usmb.android_witube.model.video.Video;

public interface IVideoAdapterCallbackListener {

    void setToolbarIcons(boolean onPlayingVideo);

    void play(Video video);

    void exitPlayer();

    void deleteVideo();
}
