package com.androidproject.usmb.android_witube.utilities.ui;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

/**
 *  App Metrics
 */
public abstract class AppMetrics {

    public static DisplayMetrics getDisplayMetrics(Activity activity){
        //get screen metrics
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return metrics;
    }

    @SuppressWarnings("unused")
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / 120);
    }
}
