package com.androidproject.usmb.android_witube.utilities;

import android.util.Log;

import org.json.JSONObject;

public class AppFunctions {

    private static final String TAG = "AppFunctions";

    /**
     * return null if json caan't find the corresponding object
     * @param jsonobject
     * @param field
     * @return
     */
    public static String getJsonString(JSONObject jsonobject, String field) {
        if(jsonobject.isNull(field))
            return "";
        else
            try {
                return jsonobject.getString(field);
            }
            catch(Exception ex) {
                Log.e(TAG, "Error parsing value");
                return null;
            }
    }

}
