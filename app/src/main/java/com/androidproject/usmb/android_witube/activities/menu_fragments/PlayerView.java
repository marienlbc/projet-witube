package com.androidproject.usmb.android_witube.activities.menu_fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.MediaCodec;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.logic.player.dash.DashRendererBuilder;
import com.androidproject.usmb.android_witube.logic.player.ui.EControlsMode;
import com.androidproject.usmb.android_witube.logic.player.other.ExtractorRendererBuilder;
import com.androidproject.usmb.android_witube.logic.player.hls.HLSRendererBuilder;
import com.androidproject.usmb.android_witube.logic.player.IRendererBuilder;
import com.androidproject.usmb.android_witube.logic.player.PlayerConstants;
import com.androidproject.usmb.android_witube.logic.player.ss.SmoothStreamingRendererBuilder;
import com.androidproject.usmb.android_witube.logic.player.ss.SmoothStreamingTestMediaDrmCallback;
import com.androidproject.usmb.android_witube.logic.player.dash.WidevineTestMediaDrmCallback;
import com.androidproject.usmb.android_witube.model.video.Video;
import com.androidproject.usmb.android_witube.utilities.ui.AppMetrics;
import com.androidproject.usmb.android_witube.utilities.ui.ExpandOrCollapseLayoutAnimation;
import com.google.android.exoplayer.DummyTrackRenderer;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.TimeRange;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.audio.AudioTrack;
import com.google.android.exoplayer.chunk.ChunkSampleSource;
import com.google.android.exoplayer.chunk.Format;
import com.google.android.exoplayer.dash.DashChunkSource;
import com.google.android.exoplayer.drm.StreamingDrmSessionManager;
import com.google.android.exoplayer.hls.HlsSampleSource;
import com.google.android.exoplayer.metadata.MetadataTrackRenderer;
import com.google.android.exoplayer.metadata.id3.Id3Frame;
import com.google.android.exoplayer.text.Cue;
import com.google.android.exoplayer.text.TextRenderer;
import com.google.android.exoplayer.upstream.BandwidthMeter;
import com.google.android.exoplayer.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.android.exoplayer.util.PlayerControl;
import com.google.android.exoplayer.util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.android.exoplayer.ExoPlayer.STATE_IDLE;
import static com.google.android.exoplayer.ExoPlayer.STATE_PREPARING;

/**
 *  Player Implementation
 */
public class PlayerView implements View.OnTouchListener, SeekBar.OnSeekBarChangeListener, View.OnClickListener,PopupMenu.OnMenuItemClickListener,
        HlsSampleSource.EventListener, MediaCodecVideoTrackRenderer.EventListener, ChunkSampleSource.EventListener, MediaCodecAudioTrackRenderer.EventListener, TextRenderer,
        MetadataTrackRenderer.MetadataRenderer<List<Id3Frame>>, StreamingDrmSessionManager.EventListener, DefaultBandwidthMeter.EventListener, DashChunkSource.EventListener{

    private View mView;
    private Context mContext;
    private Activity mActivity;

    private RelativeLayout mRvPlayerView;
    private RelativeLayout mRoot;
    private RelativeLayout mLoadingPanel;
    private LinearLayout mUnlockPanel;

    private SeekBar mSeekBar;
    private TextView mTxtVideoTitle;
    private TextView mTxtCt;
    private TextView mTxtTd;
    private ImageButton mBtnPlay;
    private ImageButton mBtnPause;
    private ImageButton mBtnPrev;
    private ImageButton mBtnNext;
    private ImageButton mBtnFastForward;
    private ImageButton mBtnRewind;
    private ImageButton mBtnLock;
    private ImageButton mBtnUnlock;
    private ImageButton mBtnSettings;

    private SurfaceView mSurfaceView;
    private ExoPlayer mPlayer;
    private PlayerControl mPlayerControl;
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private View mDecorView;

    private Handler mainHandler;
    private TrackRenderer mTrackRenderer;
    private EControlsMode mControlsMode;
    private static Video mVideoInstance;

    private ExpandOrCollapseLayoutAnimation mAnimationManager;

    private ArrayList<Object> mVideoQuality;

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private int mUiImmersiveOptions;
    private int mCurrentTrackIndex;

    public static final int TYPE_VIDEO = 0;
    public static final int TYPE_AUDIO = 1;
    public static final int RENDERER_COUNT = 3;
    public static final int TYPE_TEXT = 2;

    private static int SPEED_MINUS_1_5 = 1;
    private static int SPEED_NORMAL = 2;
    private static int SPEED_PLUS_1_5 = 3;
    private static int PLAYER_COLLAPSED_SIZE = 0;

    private static int selectedQuality = 1;
    private static Object selectedSpeed = null;

    @SuppressWarnings("WeakerAccess")
    public static boolean mIsPlayerViewOpen = false;

    @SuppressWarnings("WeakerAccess")
    public PlayerView (Activity activity, Context context, View view){
        this.mView = view;
        this.mContext = context;
        this.mActivity = activity;
        PLAYER_COLLAPSED_SIZE = (int)((float)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, AppMetrics.getDisplayMetrics(mActivity)));

        initViewComponents();
        initVideoConfig();
        setComponentsListeners();

        if(mIsPlayerViewOpen) openPlayerView();
    }

    private void initViewComponents() {
        mAnimationManager = new ExpandOrCollapseLayoutAnimation();

        mRvPlayerView = (RelativeLayout) mView.findViewById(R.id.rv_flv_player_view);
        mRvPlayerView = (RelativeLayout) mView.findViewById(R.id.rv_flv_player_view);
        mRoot = (RelativeLayout) mView.findViewById(R.id.root);
        mLoadingPanel = (RelativeLayout) mView.findViewById(R.id.loadingVPanel);
        mUnlockPanel = (LinearLayout) mView.findViewById(R.id.unlock_panel);

        mRvPlayerView.getLayoutParams().height = PLAYER_COLLAPSED_SIZE;

        //exo player
        mSurfaceView = (SurfaceView) mView.findViewById(R.id.surface_view);

        mSeekBar = (SeekBar) mView.findViewById(R.id.seekbar);

        mTxtVideoTitle = (TextView) mView.findViewById(R.id.video_title);
        mTxtCt = (TextView) mView.findViewById(R.id.txt_currentTime);
        mTxtTd = (TextView) mView.findViewById(R.id.txt_totalDuration);
        mBtnPlay = (ImageButton) mView.findViewById(R.id.control_btn_play);
        mBtnPause = (ImageButton) mView.findViewById(R.id.control_btn_pause);
        mBtnPrev = (ImageButton) mView.findViewById(R.id.control_btn_previous_video);
        mBtnNext = (ImageButton) mView.findViewById(R.id.control_btn_next_video);
        mBtnFastForward = (ImageButton) mView.findViewById(R.id.control_btn_ff);
        mBtnRewind = (ImageButton) mView.findViewById(R.id.control_btn_rewind);
        mBtnLock = (ImageButton) mView.findViewById(R.id.ib_locked_video_view);
        mBtnUnlock = (ImageButton) mView.findViewById(R.id.ib_unlocked_video_view);
        mBtnSettings = (ImageButton) mView.findViewById(R.id.control_btn_settings);

    }

    private void setComponentsListeners() {
        mSurfaceView.setOnTouchListener(this);
        mSeekBar.setOnSeekBarChangeListener(this);

        mBtnPlay.setOnClickListener(this);
        mBtnPause.setOnClickListener(this);
        mBtnPrev.setOnClickListener(this);
        mBtnFastForward.setOnClickListener(this);
        mBtnRewind.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
        mBtnLock.setOnClickListener(this);
        mBtnUnlock.setOnClickListener(this);
        mBtnSettings.setOnClickListener(this);
    }

    private void initVideoConfig() {

        mainHandler = new Handler();
        mVideoInstance = new Video();

        mVideoQuality = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mUiImmersiveOptions = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }else{
            mUiImmersiveOptions = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            );
        }

        mDecorView = mActivity.getWindow().getDecorView();
        //mDecorView.setSystemUiVisibility(mUiImmersiveOptions);

        mCurrentTrackIndex = 0;
    }

    //region Player View

    @SuppressWarnings("WeakerAccess")
    public boolean closePlayerView(){
        if (mIsPlayerViewOpen) {
            mAnimationManager.collapse(mRvPlayerView, PLAYER_COLLAPSED_SIZE);
            mIsPlayerViewOpen = false;
            killPlayer();
            return true;
        }
        return  false;
    }

    private boolean openPlayerView(){
        if (!mIsPlayerViewOpen){
            mAnimationManager.expand(mRvPlayerView, AppMetrics.getDisplayMetrics(mActivity).heightPixels);
            mIsPlayerViewOpen = true;
            execute();
            return true;
        }
        return false;
    }

    @SuppressWarnings("WeakerAccess")
    public void play(Video video, int videoIndex){
        mVideoInstance = video;
        mCurrentTrackIndex = videoIndex;
        openPlayerView();
    }

    @SuppressWarnings("WeakerAccess")
    public void exitPlayer(){
        mVideoInstance = null;
        closePlayerView();
    }

    private void hideAllControls(){
        if(mControlsMode == EControlsMode.FULLCONTORLS){
            if(mRoot.getVisibility()==View.VISIBLE){
                mRoot.setVisibility(View.GONE);
            }
        }
        else if(mControlsMode == EControlsMode.LOCK){
            if(mUnlockPanel.getVisibility()==View.VISIBLE){
                mUnlockPanel.setVisibility(View.GONE);
            }
        }
        // mDecorView.setSystemUiVisibility(mUiImmersiveOptions);
    }

    private void showControls(){
        if(mControlsMode == EControlsMode.FULLCONTORLS){
            if(mRoot.getVisibility()==View.GONE){
                mRoot.setVisibility(View.VISIBLE);
            }
        }
        else if(mControlsMode == EControlsMode.LOCK){
            if(mUnlockPanel.getVisibility()==View.GONE){
                mUnlockPanel.setVisibility(View.VISIBLE);
            }
        }
        mainHandler.removeCallbacks(hideControls);
        mainHandler.postDelayed(hideControls, 3000);
    }

    @SuppressWarnings("WeakerAccess")
    @SuppressLint("DefaultLocale")
    public String getCurrentPlayingTime(){
        return mTxtCt.getText().toString();
    }

    @SuppressWarnings("WeakerAccess")
    public Video getVideoInstance(){
        if(mVideoInstance == null) return new Video();
        return mVideoInstance;
    }

    @SuppressWarnings("unused")
    public void setPlayerVisibility(boolean visible) {
        if(!visible) mRvPlayerView.setVisibility(View.GONE);
        else {
            mRvPlayerView.setVisibility(View.VISIBLE);
            mRvPlayerView.getLayoutParams().height = PLAYER_COLLAPSED_SIZE;
        }
    }

    //endregion

    //region ExoPlayer controls

    /**
     *  Play the video
     */
    private void execute() {
        mPlayer = ExoPlayer.Factory.newInstance(RENDERER_COUNT);
        mPlayerControl = new PlayerControl(mPlayer);

        //avoid out of bounds
        if(mCurrentTrackIndex >= VideoGalleryFragment.videos.size()){
            mCurrentTrackIndex =(VideoGalleryFragment.videos.size()-1);
        }else if(mCurrentTrackIndex <=0){
            mCurrentTrackIndex =0;
        }

        mVideoInstance = VideoGalleryFragment.videos.get(mCurrentTrackIndex);

        mTxtVideoTitle.setText(mVideoInstance.getTitle());

        if(mPlayer!=null) {
            try {
                IRendererBuilder rendererBuilder = getRendererBuilder();
                rendererBuilder.buildRenderer(this);
                mLoadingPanel.setVisibility(View.VISIBLE);
                mainHandler.postDelayed(updatePlayer, 200);
                mainHandler.postDelayed(hideControls, 3000);
                mControlsMode = EControlsMode.FULLCONTORLS;
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(mContext, "Playback error", Toast.LENGTH_SHORT).show();
                killPlayer();
            }
        }
    }

    /**
     * reset video instance and close the player view
     */
    private void killPlayer(){
        if (mPlayer != null) {
            mPlayer.release();
            mVideoInstance = null;
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void resume(){
        if(!mPlayerControl.isPlaying()){
            mPlayerControl.start();
            mBtnPause.setVisibility(View.VISIBLE);
            mBtnPlay.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void pause(){
        if(mPlayerControl.isPlaying()){
            mPlayerControl.pause();
            mBtnPause.setVisibility(View.GONE);
            mBtnPlay.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Build the video renderer depending on its format
     * @return IRendererBuilder
     * @throws Exception Unsupported video Type
     */
    private IRendererBuilder getRendererBuilder() throws Exception {
        String userAgent = Util.getUserAgent(mContext, "HpLib");
        switch (mVideoInstance.getType()){
            case PlayerConstants.HLS:
                return new HLSRendererBuilder(mContext, userAgent, mVideoInstance.getUrl());
            case PlayerConstants.SS:
                return new SmoothStreamingRendererBuilder(mContext, userAgent, mVideoInstance.getUrl(),
                        new SmoothStreamingTestMediaDrmCallback());
            case PlayerConstants.DASH:
                return new DashRendererBuilder(mContext, userAgent,mVideoInstance.getUrl(),
                        new WidevineTestMediaDrmCallback(mVideoInstance.getContentId(), mVideoInstance.getProvider()));
            case PlayerConstants.OTHER:
                return new ExtractorRendererBuilder(mContext,userAgent, Uri.parse(mVideoInstance.getUrl()));
            default:
                throw new IllegalStateException("Unsupported type: " + mVideoInstance.getType() + " for " + mVideoInstance.getUrl());
        }
    }

    public Looper getPlaybackLooper() { return mPlayer.getPlaybackLooper(); }

    public Handler getMainHandler() { return mainHandler; }

    /**
     *  Renderer error callback
     * @param e Exception message
     */
    public void onRendererError(Exception e) {
        e.printStackTrace();
        if(e.getMessage() != null){
            Log.e("PlayerView", e.getMessage());
            Toast.makeText(mContext, "Rendrer Error : " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Renderer Callback
     * @param renderer track renderer
     * @param bandwidthMeter measure bandwidth, optional
     */
    public void onRenderer(TrackRenderer[] renderer, @SuppressWarnings("UnusedParameters") BandwidthMeter bandwidthMeter) {
        for (int i = 0; i < renderer.length; i++) {
            if (renderer[i] == null) {
                renderer[i] = new DummyTrackRenderer();
            }
        }
        // Complete preparation.
        this.mTrackRenderer = renderer[TYPE_VIDEO];
        pushSurface(false);
        mPlayer.prepare(renderer);
        mPlayer.setPlayWhenReady(true);
    }

    /**
     *  Push to Surface View
     * @param blockForSurfacePush boolean
     */
    private void pushSurface(boolean blockForSurfacePush) {
        if (mTrackRenderer == null) {return;}
        if (blockForSurfacePush) {
            mPlayer.blockingSendMessage(
                    mTrackRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, mSurfaceView.getHolder().getSurface());
        } else {
            mPlayer.sendMessage(
                    mTrackRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, mSurfaceView.getHolder().getSurface());
        }
    }

    /**
     * settings menu of the player view
     * @param v context View
     * @return PopupMenu
     */
    private PopupMenu settingsMenu(View v){
        PopupMenu popupMenu = new PopupMenu(mContext, v);
        Menu menu = popupMenu.getMenu();
        mVideoQuality.clear();

        SubMenu videoQualitySubMenu = menu.addSubMenu("Video Quality");

        for (int i = 0; i < mPlayer.getTrackCount(0); i++) {
            MediaFormat format = mPlayer.getTrackFormat(0, i);
            if (MimeTypes.isVideo(format.mimeType)) {
                if (format.adaptive) {
                    videoQualitySubMenu.add(0, (i + 1), (i + 1), "Auto");
                    mVideoQuality.add((i+1));
                } else {
                    videoQualitySubMenu.add(0, (i + 1), (i + 1), format.width + "p");
                    mVideoQuality.add((i+1));
                }
            }
        }

        videoQualitySubMenu.setGroupCheckable(0, true, true);
        videoQualitySubMenu.findItem(selectedQuality).setChecked(true);

        // Handle menu items index
        SPEED_MINUS_1_5 = mVideoQuality.size() + 1;
        SPEED_NORMAL = SPEED_MINUS_1_5 + 1;
        SPEED_PLUS_1_5 = SPEED_NORMAL + 1;

        SubMenu videoSpeedSubMenu = menu.addSubMenu("Video Speed");
        videoSpeedSubMenu.add(1,SPEED_MINUS_1_5,0, "-1.5x");
        videoSpeedSubMenu.add(1,SPEED_NORMAL,1, "0x");
        videoSpeedSubMenu.add(1,SPEED_PLUS_1_5,2, "+1.5x");

        videoSpeedSubMenu.setGroupCheckable(1, true, true);
        if(selectedSpeed == null){
            selectedSpeed = SPEED_NORMAL;
            videoSpeedSubMenu.findItem((int)selectedSpeed).setChecked(true);
        }
        else{
            videoSpeedSubMenu.findItem((int)selectedSpeed).setChecked(true);
        }
        return popupMenu;
    }

    //endregion

    //region Runnable

    private Runnable updatePlayer, hideControls;
    {
        updatePlayer = new Runnable() {
            @Override
            public void run() {
                switch (mPlayer.getPlaybackState()) {
                    case ExoPlayer.STATE_BUFFERING:
                        mLoadingPanel.setVisibility(View.VISIBLE);
                        break;
                    case ExoPlayer.STATE_ENDED:
                        openPlayerView(); //TODO
                        //openVideoView();
                        break;
                    case STATE_IDLE:
                        mLoadingPanel.setVisibility(View.GONE);
                        break;
                    case STATE_PREPARING:
                        mLoadingPanel.setVisibility(View.VISIBLE);
                        break;
                    case ExoPlayer.STATE_READY:
                        mLoadingPanel.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }

                //seekBar and time duration
                @SuppressLint("DefaultLocale")
                String totDur = String.format("%02d.%02d.%02d",
                        TimeUnit.MILLISECONDS.toHours(mPlayer.getDuration()),
                        TimeUnit.MILLISECONDS.toMinutes(mPlayer.getDuration()) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mPlayer.getDuration())), // The change is in this line
                        TimeUnit.MILLISECONDS.toSeconds(mPlayer.getDuration()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mPlayer.getDuration())));
                @SuppressLint("DefaultLocale")
                String curDur = String.format("%02d.%02d.%02d",
                        TimeUnit.MILLISECONDS.toHours(mPlayer.getCurrentPosition()),
                        TimeUnit.MILLISECONDS.toMinutes(mPlayer.getCurrentPosition()) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mPlayer.getCurrentPosition())), // The change is in this line
                        TimeUnit.MILLISECONDS.toSeconds(mPlayer.getCurrentPosition()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mPlayer.getCurrentPosition())));

                mTxtCt.setText(curDur);
                mTxtTd.setText(totDur);
                mSeekBar.setMax((int) mPlayer.getDuration());
                mSeekBar.setProgress((int) mPlayer.getCurrentPosition());

                mainHandler.postDelayed(updatePlayer, 200);
            }
        };
    }
    {
        hideControls = new Runnable() {
            @Override
            public void run() {
                hideAllControls();
            }
        };
    }

    //endregion

    //region Listeners

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (MotionEventCompat.getActionMasked(event)){
            case MotionEvent.ACTION_UP: // on tap
                showControls();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {mPlayer.seekTo(seekBar.getProgress());}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.control_btn_pause :
                if (mPlayerControl.isPlaying()) {
                    pause();
                }
                break;
            case R.id.control_btn_play :
                if (!mPlayerControl.isPlaying()) {
                    resume();
                }
                break;
            case R.id.control_btn_next_video :
                mPlayer.release();
                selectedSpeed = null;
                selectedQuality = 1;
                mCurrentTrackIndex++;
                execute();
                break;
            case R.id.control_btn_previous_video :
                mPlayer.release();
                selectedSpeed = null;
                selectedQuality = 1;
                mCurrentTrackIndex--;
                execute();
                break;
            case R.id.control_btn_ff :
                mPlayer.seekTo(mPlayer.getCurrentPosition() + 5000);
                break;
            case R.id.control_btn_rewind :
                mPlayer.seekTo(mPlayer.getCurrentPosition() - 5000);
                break;
            case R.id.ib_locked_video_view :
                mControlsMode = EControlsMode.FULLCONTORLS;
                mRoot.setVisibility(View.VISIBLE);
                mBtnLock.setVisibility(View.GONE);
                mBtnUnlock.setVisibility(View.VISIBLE);
                mUnlockPanel.setVisibility(View.GONE);
                break;
            case R.id.ib_unlocked_video_view :
                mControlsMode = EControlsMode.LOCK;
                mRoot.setVisibility(View.GONE);
                mBtnUnlock.setVisibility(View.GONE);
                mBtnLock.setVisibility(View.VISIBLE);
                mUnlockPanel.setVisibility(View.VISIBLE);
                break;
            case R.id.control_btn_settings:
                PopupMenu popupMenu = settingsMenu(v);
                popupMenu.setOnMenuItemClickListener(this);
                popupMenu.show();
                break;
            default:
                break;
        }

    }

    @Override
    public void onLoadStarted(int sourceId, long length, int type, int trigger, Format format, long mediaStartTimeMs, long mediaEndTimeMs) {}

    @Override
    public void onLoadCompleted(int sourceId, long bytesLoaded, int type, int trigger, Format format, long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs) {}

    /**
     * reset instances, close Player View
     * @param sourceId sourceId
     * @param bytesLoaded bytesLoaded
     */
    @Override
    public void onLoadCanceled(int sourceId, long bytesLoaded) {
        mVideoInstance = null;
        closePlayerView();
        Toast.makeText(mContext, "Error, load canceled", Toast.LENGTH_SHORT).show();
    }

    /**
     * notify error load, close Player View
     * @param sourceId source id
     * @param e IOException
     */
    @Override
    public void onLoadError(int sourceId, IOException e) {
        e.printStackTrace();
        if(e.getMessage() != null) Log.e("PlayerView", e.getMessage());
        mVideoInstance = null;
        closePlayerView();
        Toast.makeText(mContext, "Error, Could not load the resource", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpstreamDiscarded(int sourceId, long mediaStartTimeMs, long mediaEndTimeMs) {}

    @Override
    public void onDownstreamFormatChanged(int sourceId, Format format, int trigger, long mediaTimeMs) {}

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if(mVideoQuality.contains(item.getItemId())){
            mPlayer.setSelectedTrack(0, (item.getItemId() -1 ));
            selectedQuality = item.getItemId();
            Toast.makeText(mContext,item.getTitle(),Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(item.getItemId() == SPEED_MINUS_1_5){
            selectedSpeed = SPEED_MINUS_1_5;
            //TODO not implemented
            //Toast.makeText(mContext,item.getTitle(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"Not available",Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(item.getItemId() == SPEED_NORMAL){
            selectedSpeed = SPEED_NORMAL;
            //TODO not implemented
            //Toast.makeText(mContext,item.getTitle(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"Not available",Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(item.getItemId() == SPEED_PLUS_1_5){
            selectedSpeed = SPEED_PLUS_1_5;
            //TODO not implemented
            //Toast.makeText(mContext,item.getTitle(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"Not available",Toast.LENGTH_SHORT).show();
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void onDroppedFrames(int count, long elapsed) {}

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {}

    @Override
    public void onDrawnToSurface(Surface surface) {}

    @Override
    public void onDecoderInitializationError(MediaCodecTrackRenderer.DecoderInitializationException e) {}

    @Override
    public void onCryptoError(MediaCodec.CryptoException e) {}

    @Override
    public void onDecoderInitialized(String decoderName, long elapsedRealtimeMs, long initializationDurationMs) {}

    @Override
    public void onAudioTrackInitializationError(AudioTrack.InitializationException e) {}

    @Override
    public void onAudioTrackWriteError(AudioTrack.WriteException e) {}

    @Override
    public void onAudioTrackUnderrun(int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {}

    @Override
    public void onMetadata(List<Id3Frame> metadata) {}

    @Override
    public void onCues(List<Cue> cues) {}

    @Override
    public void onBandwidthSample(int elapsedMs, long bytes, long bitrate) {}

    @Override
    public void onDrmKeysLoaded() {}

    @Override
    public void onDrmSessionManagerError(Exception e) {}

    @Override
    public void onAvailableRangeChanged(int sourceId, TimeRange availableRange) {}


    //endregion

}
