package com.androidproject.usmb.android_witube.logic.player;

public abstract class PlayerConstants {
    public static final String DASH = "dash";
    public static final String SS = "ss";
    public static final String HLS = "hls";
    public static final String OTHER = "others";
}
