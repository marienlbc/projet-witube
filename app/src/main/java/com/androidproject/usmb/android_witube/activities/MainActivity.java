package com.androidproject.usmb.android_witube.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.activities.entry.RootActivity;
import com.androidproject.usmb.android_witube.activities.menu_fragments.AboutFragment;
import com.androidproject.usmb.android_witube.activities.menu_fragments.AddVideoFragment;
import com.androidproject.usmb.android_witube.activities.menu_fragments.VideoGalleryFragment;
import com.androidproject.usmb.android_witube.model.user.UserSession;
import com.androidproject.usmb.android_witube.utilities.storage.SharedPreferenceHelper;
import com.androidproject.usmb.android_witube.utilities.ui.CircleTransform;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 *  Home Activity, implements Navigatio Drawer and handle Fragments
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG_HOME = "home";
    private static final String TAG_ADD_VIDEO = "add video";
    private static final String TAG_ABOUT = "about";
    private static final String TAG_LOGOUT = "logout";

    private NavigationView mNavigationView;
    private DrawerLayout mDrawer;
    private ImageView mImgNavHeaderBg, mNavHeaderImgProfile;
    private TextView mNavHeaderUserName;

    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_HOME;

    //toolbar titles respected to selected nav menu item
    private String[] mActivityTitles;
    private Toolbar topToolBar;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        topToolBar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);

        //topToolBar.setLogo(R.drawable.wiitube);
        topToolBar.setLogoDescription(getResources().getString(R.string.logo_desc));
        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(NullPointerException npe){
            npe.printStackTrace();
        }

        mHandler = new Handler();

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        View navHeader = mNavigationView.getHeaderView(0);
        mNavHeaderUserName = (TextView) navHeader.findViewById(R.id.tv_nav_header_user_name);
        mImgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        mNavHeaderImgProfile = (ImageView) navHeader.findViewById(R.id.tv_nav_header_img_profile);

        mActivityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }

    }

    //region Navigation Drawer

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        mNavHeaderUserName.setText(UserSession.getInstance().getUsername());

        // loading header background image
        Glide.with(this).load(R.drawable.wiitube_logo)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mImgNavHeaderBg);

        // Loading profile image
        Glide.with(this).load(UserSession.getInstance().getGravatarUrl())
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mNavHeaderImgProfile);
    }


    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            mDrawer.closeDrawers();

            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.home_fragment_container, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        //noinspection ConstantConditions
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        //Closing drawer on item click
        mDrawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private void setToolbarTitle() {
        try{
            getSupportActionBar().setTitle(mActivityTitles[navItemIndex]);
        } catch(NullPointerException npe){
            npe.printStackTrace();
        }
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                return new VideoGalleryFragment();
            case 1:
                //add new video
                return new AddVideoFragment();
            case 2:
                //about
                return new AboutFragment();
            case 3:
                //logout
                SharedPreferenceHelper.clearUserSession(this);
                UserSession.resetInstance();
                Intent loginIntent = new Intent(this, RootActivity.class);
                startActivity(loginIntent);
            default:
                return new VideoGalleryFragment();
        }
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.add_video:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_ADD_VIDEO;
                        break;
                    case R.id.about:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_ABOUT;
                        break;
                    case R.id.logout:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_LOGOUT;
                        break;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, topToolBar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        mDrawer.setDrawerListener(mActionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        mActionBarDrawerToggle.syncState();
    }

    private void selectNavMenu() { mNavigationView.getMenu().getItem(navItemIndex).setChecked(true);}

    //endregion

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        boolean mShouldLoadHomeFragOnBackPress = true;
        //noinspection ConstantConditions
        if (mShouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

}
