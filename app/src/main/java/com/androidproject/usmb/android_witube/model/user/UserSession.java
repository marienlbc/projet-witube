package com.androidproject.usmb.android_witube.model.user;



public class UserSession extends User {

    private static UserSession UserSessionInstance = null;

    public static final String USER_NOT_FOUND = "User not found";
    public static final String BAD_CREDENTIALS = "Bad credentials";

    private UserSession() { super(); }

    /** Point d'accès pour l'instance unique du singleton */
    public synchronized static UserSession getInstance() {
        if (UserSessionInstance == null){
            UserSessionInstance = new UserSession();
        }
        return UserSessionInstance;
    }

    public synchronized static void resetInstance(){ UserSessionInstance = null; }



}
