package com.androidproject.usmb.android_witube.activities.entry;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.androidproject.usmb.android_witube.AppController;
import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.activities.MainActivity;
import com.androidproject.usmb.android_witube.logic.api.APIConstants;
import com.androidproject.usmb.android_witube.logic.api.APIHelper;
import com.androidproject.usmb.android_witube.logic.user.UserHelper;
import com.androidproject.usmb.android_witube.model.user.User;
import com.androidproject.usmb.android_witube.model.user.UserSession;
import com.androidproject.usmb.android_witube.utilities.AppFunctions;
import com.androidproject.usmb.android_witube.utilities.storage.SharedPreferenceHelper;
import com.androidproject.usmb.android_witube.utilities.volley.CustomJSONArrayRequest;
import com.androidproject.usmb.android_witube.utilities.volley.VolleyErrorHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Root of the application, check which workflow to start depending on user's connection state
 *
 */

public class RootActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener {

    private static final String TAG = "RootActivity";

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        mProgressDialog = new ProgressDialog(this);

        if(SharedPreferenceHelper.getUserSession(this).length() == 0) launchEntryPage();
        else launchHomePage();

    }

    private void launchHomePage() {
        mProgressDialog.setMessage("Connecting....");
        mProgressDialog.show();

        CustomJSONArrayRequest loginRequest  = new CustomJSONArrayRequest(Request.Method.GET, APIConstants.LOGIN_URL, this, this);

        loginRequest.setTag(TAG); // to identify the request object
        AppController.getInstance().addToRequestQueue(loginRequest);   // Access the RequestQueue through singleton class.
    }

    private void launchEntryPage() {

        //Launch Front Page Fragment
        EntryFragment front_page_fragment = new EntryFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.ll_ar_root, front_page_fragment);
        transaction.commit();

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.e(VolleyErrorHelper.getMessage(error, this));
        mProgressDialog.dismiss();
        //Toast.makeText(this, "User Connexion Error", Toast.LENGTH_SHORT).show();
        launchEntryPage();
    }

    @Override
    public void onResponse(Object response) {
        try{
            User requestLogin = new User();

            //get user session from Shared Preference as a Json
            JSONObject jUserSession = new JSONObject(SharedPreferenceHelper.getUserSession(this));
            if(jUserSession.length() == 0){
                throw new Exception("jUserSession empty");
            }
            requestLogin.setUsername(AppFunctions.getJsonString(jUserSession,APIConstants.USER_LABEL_USERNAME));
            requestLogin.setPassword(AppFunctions.getJsonString(jUserSession,APIConstants.USER_LABEL_PASSWORD));

            User u = UserHelper.checkLogin(requestLogin, APIHelper.parseGetUsersResponse((JSONArray) response));
            if(u != null){

                //set user session (singleton)
                UserSession userSession = UserSession.getInstance();
                userSession.setId(u.getId());
                userSession.setUsername(u.getUsername());
                userSession.setPassword(u.getPassword());
                userSession.setGravatarUrl(u.getGravatarUrl());
                userSession.setEmail(u.getEmail());

                //update user session
                SharedPreferenceHelper.setUserSession(this, UserHelper.convertUserToString(userSession));

                mProgressDialog.dismiss();

                Intent home = new Intent(this, MainActivity.class);
                startActivity(home);
            }else{
                Toast.makeText(this, UserSession.BAD_CREDENTIALS + " - Please re-enter your credentials", Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
                launchEntryPage();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "parsing Shared Preference User : " + e.getMessage());
            Toast.makeText(this, "User Connexion Error", Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
            launchEntryPage();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Global Exception : " + e.getMessage());
            Toast.makeText(this, UserSession.USER_NOT_FOUND + " - User Connexion Error", Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
            launchEntryPage();
        }
    }

}
