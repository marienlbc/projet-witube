package com.androidproject.usmb.android_witube.logic.video;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidproject.usmb.android_witube.R;

@SuppressWarnings("WeakerAccess")
public class VideoItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public ImageView mVideoThumbnail;
    public ImageButton mDeleteVideo;
    public ImageButton mPlayVideo;
    public TextView mVideoTitle;

    private IVideoAdapterClickListener mRecyclerViewItemClickListener;

    public VideoItemViewHolder(View itemView) {
        super(itemView);

        mVideoTitle = (TextView) itemView.findViewById(R.id.tv_title);
        mVideoThumbnail = (ImageView)itemView.findViewById(R.id.iv_video_thumbnail);
        mDeleteVideo = (ImageButton) itemView.findViewById(R.id.ib_delete);
        mPlayVideo = (ImageButton) itemView.findViewById(R.id.ib_play);

        mVideoThumbnail.setOnClickListener(this);
        mPlayVideo.setOnClickListener(this);
        mDeleteVideo.setOnClickListener(this);
        mVideoTitle.setOnClickListener(this);
    }

    public void setClickListener(IVideoAdapterClickListener listener) {
        this.mRecyclerViewItemClickListener = listener;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.iv_video_thumbnail:
                mRecyclerViewItemClickListener.play(getAdapterPosition());
                break;
            case R.id.tv_title:
                mRecyclerViewItemClickListener.play(getAdapterPosition());
                break;
            case R.id.ib_play:
                mRecyclerViewItemClickListener.play(getAdapterPosition());
                break;
            case R.id.ib_delete:
                mRecyclerViewItemClickListener.deleteVideo(getAdapterPosition());
                break;
            default:
                break;
        }
    }
}
