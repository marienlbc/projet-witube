package com.androidproject.usmb.android_witube.logic.player.ui;

public enum EPlaybackState {
    PLAYING, PAUSED, BUFFERING, IDLE
}
