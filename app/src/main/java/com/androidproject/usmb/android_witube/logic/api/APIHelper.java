package com.androidproject.usmb.android_witube.logic.api;

import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.model.user.User;
import com.androidproject.usmb.android_witube.model.user.UserSession;
import com.androidproject.usmb.android_witube.model.video.Video;
import com.androidproject.usmb.android_witube.utilities.AppFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Map;

public abstract class APIHelper {


    /**
     * buildNewVideoAnnotationRequest
     * @param videos JSONArray
     * @param videoId String
     * @param newAnnotation String
     * @return JSONArray
     * @throws JSONException json format exception
     */
    @SuppressWarnings("unused")
    public static JSONArray buildNewVideoAnnotationRequest(JSONArray videos, String videoId, String newAnnotation) throws JSONException {

        for(int i = 0; i < videos.length(); i++){
            if(videos.getJSONObject(i).getString(APIConstants.VIDEO_LABEL_ID).equals(videoId)) {
                videos.getJSONObject(i).put(APIConstants.VIDEO_LABEL_COMMENT, newAnnotation);
            }
        }

        return videos;
    }

    /**
     *
     * @param videosJSONArrayResponse videos list in json array format
     * @return ArrayList<Video>
     * @throws JSONException json exception
     */
    public static ArrayList<Video> parseGetVideosResponse(JSONArray videosJSONArrayResponse) throws JSONException {

        ArrayList<Video> videos = new ArrayList<>();

        for(int i = 0; i < videosJSONArrayResponse.length(); i++){

            Video v = new Video();

            JSONObject jsonobject = videosJSONArrayResponse.getJSONObject(i);

            String id = AppFunctions.getJsonString(jsonobject, APIConstants.VIDEO_LABEL_USER_ID);
            if(UserSession.getInstance().getId().equals(id)) {

                v.setId(AppFunctions.getJsonString(jsonobject, APIConstants.VIDEO_LABEL_ID));
                v.setTitle(AppFunctions.getJsonString(jsonobject, APIConstants.VIDEO_LABEL_TITLE));
                v.setUrl(AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_URL));
                v.setDate(AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_DATE));
                v.setComment(AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_COMMENT));
                v.setProvider(AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_PROVIDER));
                v.setContentId(AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_CONTENT_ID));
                v.setType(AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_TYPE));
                v.setSource(AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_SOURCE));
                v.setUser_id(id);
                String videoThumbnail = AppFunctions.getJsonString(jsonobject,APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL);
                if (videoThumbnail != null) {
                    if(!videoThumbnail.equals("") && !videoThumbnail.matches("-?\\d+(\\.\\d+)?")) v.setVideoThumbnail(videoThumbnail);
                    else v.setVideoThumbnail(R.drawable.wiideo_default_thumbnail + "");
                }

                ArrayList<String> tagsList = new ArrayList<>();

               JSONArray jArrayTags = jsonobject.getJSONArray(APIConstants.VIDEO_LABEL_TAGS);

                for(int j = 0; j < jArrayTags.length(); j++){
                        tagsList.add(jArrayTags.getString(j));
                }

                v.setTags(tagsList);
                videos.add(v);

            }
        }

        return videos;
    }

    /**
     * parse volley response
     * @param usersJSONArrayResponse users list in json array format
     * @return ArrayList<User>
     * @throws JSONException json exception
     */
    public static ArrayList<User> parseGetUsersResponse(JSONArray usersJSONArrayResponse) throws JSONException {

        ArrayList<User> users = new ArrayList<>();

        for(int i = 0; i < usersJSONArrayResponse.length(); i++){

            User u = new User();

            JSONObject jsonobject = usersJSONArrayResponse.getJSONObject(i);

            u.setId(AppFunctions.getJsonString(jsonobject, APIConstants.USER_LABEL_ID));
            u.setUsername(AppFunctions.getJsonString(jsonobject, APIConstants.USER_LABEL_USERNAME));
            u.setPassword(AppFunctions.getJsonString(jsonobject, APIConstants.USER_LABEL_PASSWORD));
            u.setEmail(AppFunctions.getJsonString(jsonobject, APIConstants.USER_LABEL_EMAIL));
            u.setGravatarUrl(AppFunctions.getJsonString(jsonobject, APIConstants.USER_LABEL_GRAVATAR_URL));

            users.add(u);
        }
        return users;
    }

    /**
     *  build json video api post
     * @param listVideos ArrayList<Map<String,String>>
     * @return JSONArray
     */
    public static JSONArray buildVideoPost(ArrayList<Map<String,String>> listVideos){

        JSONArray JArrayVideo = new JSONArray();

        for (Map<String, String> video: listVideos) {
            try {
                JSONObject jvideo = new JSONObject();
                jvideo.put(APIConstants.VIDEO_LABEL_ID, video.get(APIConstants.VIDEO_LABEL_ID));
                jvideo.put(APIConstants.VIDEO_LABEL_TITLE, video.get(APIConstants.VIDEO_LABEL_TITLE));
                jvideo.put(APIConstants.VIDEO_LABEL_DATE, video.get(APIConstants.VIDEO_LABEL_DATE));
                jvideo.put(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL, video.get(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL));
                jvideo.put(APIConstants.VIDEO_LABEL_CONTENT_ID, video.get(APIConstants.VIDEO_LABEL_CONTENT_ID));
                jvideo.put(APIConstants.VIDEO_LABEL_TYPE, video.get(APIConstants.VIDEO_LABEL_TYPE));
                jvideo.put(APIConstants.VIDEO_LABEL_SOURCE, video.get(APIConstants.VIDEO_LABEL_SOURCE));
                jvideo.put(APIConstants.VIDEO_LABEL_USER_ID, video.get(APIConstants.VIDEO_LABEL_USER_ID));
                jvideo.put(APIConstants.VIDEO_LABEL_URL, video.get(APIConstants.VIDEO_LABEL_URL));

                JSONArray tags = new JSONArray();
                String[] getTags = video.get(APIConstants.VIDEO_LABEL_TAGS).split(",");
                for(String tag : getTags) {
                   tags.put(tag.replaceAll("\\s+",""));
                }

                jvideo.put(APIConstants.VIDEO_LABEL_TAGS, tags);

                JArrayVideo.put(jvideo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return JArrayVideo;
    }

    /**
     * build json video api post
     * @param listVideos ArrayList<Video>
     * @return JSONArray
     */
    public static JSONArray buildVideoFromVideo(ArrayList<Video> listVideos){

        JSONArray JArrayVideo = new JSONArray();

        for (Video video: listVideos) {
            try {
                JSONObject jvideo = new JSONObject();
                jvideo.put(APIConstants.VIDEO_LABEL_ID, video.getId());
                jvideo.put(APIConstants.VIDEO_LABEL_TITLE, video.getTitle());
                jvideo.put(APIConstants.VIDEO_LABEL_DATE, video.getDate());
                jvideo.put(APIConstants.VIDEO_LABEL_COMMENT, video.getComment());
                jvideo.put(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL, video.getVideoThumbnail());
                jvideo.put(APIConstants.VIDEO_LABEL_CONTENT_ID, video.getContentId());
                jvideo.put(APIConstants.VIDEO_LABEL_TYPE, video.getType());
                jvideo.put(APIConstants.VIDEO_LABEL_SOURCE, video.getSource());
                jvideo.put(APIConstants.VIDEO_LABEL_USER_ID, video.getUser_id());
                jvideo.put(APIConstants.VIDEO_LABEL_URL, video.getUrl());

                JSONArray tags = new JSONArray();
                for(String tag : video.getTags()) {
                    tags.put(tag);
                }
                jvideo.put(APIConstants.VIDEO_LABEL_TAGS, tags);

                JArrayVideo.put(jvideo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return JArrayVideo;
    }
}
