package com.androidproject.usmb.android_witube.activities.entry;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.androidproject.usmb.android_witube.AppController;
import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.activities.MainActivity;
import com.androidproject.usmb.android_witube.logic.api.APIConstants;
import com.androidproject.usmb.android_witube.logic.api.APIHelper;
import com.androidproject.usmb.android_witube.logic.user.UserHelper;
import com.androidproject.usmb.android_witube.model.user.User;
import com.androidproject.usmb.android_witube.model.user.UserSession;
import com.androidproject.usmb.android_witube.utilities.storage.SharedPreferenceHelper;
import com.androidproject.usmb.android_witube.utilities.volley.CustomJSONArrayRequest;
import com.androidproject.usmb.android_witube.utilities.volley.VolleyErrorHelper;

import org.json.JSONArray;
import org.json.JSONException;

/**
 *  User Login Page
 */
public class LoginFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener{

    private static final String TAG = LoginFragment.class.getSimpleName();
    private EditText mEditTextUsername;
    private EditText mEditTextPassword;
    private ProgressDialog mProgressDialog;

    public LoginFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mEditTextUsername = (EditText) view.findViewById(R.id.et_fsi_username);
        mEditTextPassword = (EditText) view.findViewById(R.id.et_fsi_password);
        Button mLogin = (Button) view.findViewById(R.id.btn_fsi_connect);
        TextView mTxtSignUp = (TextView) view.findViewById(R.id.txt_fsi_sign_up);

        mLogin.setOnClickListener(this);
        mTxtSignUp.setOnClickListener(this);

        mProgressDialog = new ProgressDialog(getActivity());

        return view;
    }


    private void login() {

        mProgressDialog.setMessage("Connecting....");
        mProgressDialog.show();

        CustomJSONArrayRequest loginRequest  = new CustomJSONArrayRequest(Request.Method.GET, APIConstants.LOGIN_URL, this, this);

        loginRequest.setTag(TAG); // to identify the request object
        AppController.getInstance().addToRequestQueue(loginRequest);   // Access the RequestQueue through singleton class.

    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.btn_fsi_connect :
                login();
                break;
            case R.id.txt_fsi_sign_up :
                Fragment signUpFragment = new RegisterFragment();
                FragmentTransaction transaction_signUpFragment = getFragmentManager().beginTransaction();
                transaction_signUpFragment.replace(R.id.ll_ar_root, signUpFragment);
                transaction_signUpFragment.addToBackStack(null);
                transaction_signUpFragment.commit();
                break;
            default:
                break;
        }

    }


    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.e(VolleyErrorHelper.getMessage(error, getContext()));
        mProgressDialog.dismiss();
        Toast.makeText(getContext(), "User Connexion Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(Object response) {

        try{
            User requestLogin = new User();

            requestLogin.setUsername(mEditTextUsername.getText().toString());
            requestLogin.setPassword(mEditTextPassword.getText().toString());

            User u = UserHelper.checkLogin(requestLogin, APIHelper.parseGetUsersResponse((JSONArray) response));
            if(u != null){

                //set user session (singleton)
                UserSession userSession = UserSession.getInstance();
                userSession.setId(u.getId());
                userSession.setUsername(u.getUsername());
                userSession.setPassword(u.getPassword());
                userSession.setGravatarUrl(u.getGravatarUrl());
                userSession.setEmail(u.getEmail());

                //update user session
                SharedPreferenceHelper.setUserSession(getContext(), UserHelper.convertUserToString(userSession));

                mProgressDialog.dismiss();

                Intent home = new Intent(getContext(), MainActivity.class);
                startActivity(home);
            }else{
                Toast.makeText(getContext(), "Please re-enter your credentials", Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "parsing response User : " + e.getMessage());
            Toast.makeText(getContext(), "User Connexion Error", Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Global Exception : " + e.getMessage());
            Toast.makeText(getContext(), "Unknown exception", Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
        }

    }
}
