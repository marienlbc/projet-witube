package com.androidproject.usmb.android_witube.activities.menu_fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.androidproject.usmb.android_witube.AppController;
import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.logic.api.APIConstants;
import com.androidproject.usmb.android_witube.logic.api.APIHelper;
import com.androidproject.usmb.android_witube.logic.player.PlayerConstants;
import com.androidproject.usmb.android_witube.logic.player.youtube.IYoutubeVideoInfoRetrieverCallback;
import com.androidproject.usmb.android_witube.logic.player.youtube.YouTubeVideoInfoRetriever;
import com.androidproject.usmb.android_witube.model.user.UserSession;
import com.androidproject.usmb.android_witube.utilities.AppFunctions;
import com.androidproject.usmb.android_witube.utilities.volley.CustomJSONArrayRequest;
import com.androidproject.usmb.android_witube.utilities.volley.VolleyErrorHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

/**
 *  Add Video Page
 */
public class AddVideoFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener,
        IYoutubeVideoInfoRetrieverCallback {

    private static final String TAG = AddVideoFragment.class.getSimpleName(); //debug
    public static final String YOUTUBE_VIDEO_NOT_FOUND = "Youtube Video Not found";

    private Fragment mFragment;

    private View mDialogView;
    private Spinner mListVideoType;
    private EditText mVideoTitle;
    private EditText mVideoUrl;
    private EditText mVideoThumbUrl;
    private EditText mVideoTags;
    private ProgressDialog mProgressDialog;

    private String mVideoType;
    private ArrayList<Map<String,String>> listVideos;

    private final Map<String, String> newVideo = new HashMap<>();

    public AddVideoFragment() {}

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_video, container, false);

        mFragment = this;

        mVideoTitle = (EditText) view.findViewById(R.id.et_fav_video_title);
        mVideoUrl = (EditText) view.findViewById(R.id.et_fav_video_url);
        mVideoThumbUrl = (EditText) view.findViewById(R.id.et_fav_video_thumb);
        mVideoTags = (EditText) view.findViewById(R.id.et_fav_video_tags);
        mDialogView = inflater.inflate(R.layout.dialog_type_video_info,null);
        mProgressDialog = new ProgressDialog(getActivity());
        Button btnAddVideo = (Button) view.findViewById(R.id.btn_fav_add_video);
        ImageButton btnInfo = (ImageButton) view.findViewById(R.id.ib_fav_info);

        /* Spinner */
        mListVideoType = (Spinner) view.findViewById(R.id.sp_fav_type_video);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.videos_type_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mListVideoType.setAdapter(adapter);

        //listeners
        mListVideoType.setOnItemSelectedListener(this);
        btnInfo.setOnClickListener(this);
        btnAddVideo.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_fav_add_video:
                addVideo();
                break;
            case R.id.ib_fav_info:
                AlertDialog.Builder addVideoAlertDialog = new AlertDialog.Builder(getContext());
                removeView(); // avoid Exception childParent
                addVideoAlertDialog.setView(mDialogView);
                TextView title = (TextView) mDialogView.findViewById(R.id.tv_tvid_title);
                TextView description = (TextView) mDialogView.findViewById(R.id.tv_tvid_description);
                String descr;
                //Toast.makeText(getContext(), mListVideoType.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                if(mListVideoType.getSelectedItem().equals("Youtube")){
                    descr = "Youtube video format. \n Get Youtube video link and paste it";
                }
                else if(mListVideoType.getSelectedItem().equals("DASH")){
                    descr = "Dynamic Adaptive Streaming over HTTP (DASH)\n" +
                            "\nDASH is an adaptive bitrate streaming technique\n" +
                            "It enables high quality streaming of media content over the Internet delivered from conventional HTTP web servers. ";
                }
                else if(mListVideoType.getSelectedItem().toString().equals("SS")){
                    descr = "Smooth Streaming is a hybrid media delivery method.\n" +
                            "\nIt acts like streaming, but is based on HTTP progressive download.\n" +
                            "The HTTP downloads are performed in a series of small chunks,\n" +
                            " allowing the media to be easily and cheaply cached along the edge of the network, closer to clients.";
                }
                else if(mListVideoType.getSelectedItem().toString().equals("HLS")){
                    descr = "HTTP Live Streaming\n" +
                            "\n HLS is an HTTP-based media streaming communications protocol implemented by Apple Inc";
                } else {
                    descr = "Any other video format, assuming that exoplayer supports it\n" +
                            "If not, the player will not play it";
                }
                title.setText(mListVideoType.getSelectedItem().toString());
                description.setText(descr);

                addVideoAlertDialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                addVideoAlertDialog.show();
                break;
            default:
                break;
        }

    }

    private void removeView(){
        if(mDialogView.getParent()!=null) {
            ((ViewGroup) mDialogView.getParent()).removeView(mDialogView);
        }
    }

    private void addVideo() {
        mProgressDialog.setMessage("Adding....");
        mProgressDialog.show();

        try {
            if(checkAddVideoForm()){
                // really bad way to do this but we don't have our own API server so we can't perform basic API operations
                //1 get all videos, add the new videos, push it all on API
                CustomJSONArrayRequest addRequest  = new CustomJSONArrayRequest(Request.Method.GET, APIConstants.WII_LIBRARY, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            listVideos =  new ArrayList<>();

                            for (int i=0; i<response.length(); i++) {
                                try {
                                    Map<String,String> video  = new HashMap<>();

                                    JSONObject jvideos = (JSONObject) response.get(i);

                                    video.put(APIConstants.VIDEO_LABEL_ID, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_ID));
                                    video.put(APIConstants.VIDEO_LABEL_TITLE, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_TITLE));
                                    video.put(APIConstants.VIDEO_LABEL_URL, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_URL));
                                    video.put(APIConstants.VIDEO_LABEL_DATE, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_DATE));
                                    video.put(APIConstants.VIDEO_LABEL_COMMENT, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_COMMENT));
                                    video.put(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL));
                                    video.put(APIConstants.VIDEO_LABEL_CONTENT_ID, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_CONTENT_ID));
                                    video.put(APIConstants.VIDEO_LABEL_TYPE, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_TYPE));
                                    video.put(APIConstants.VIDEO_LABEL_SOURCE, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_SOURCE));
                                    video.put(APIConstants.VIDEO_LABEL_USER_ID, AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_USER_ID));

                                    JSONArray jtags = new JSONArray(AppFunctions.getJsonString(jvideos,APIConstants.VIDEO_LABEL_TAGS));
                                    String tags = "";
                                    for(int k=0; k<jtags.length(); k++){
                                        tags += jtags.getString(k);
                                        if(jtags.length() > 0 && k < jtags.length() - 1) tags += ",";
                                    }
                                    video.put(APIConstants.VIDEO_LABEL_TAGS, tags);

                                    listVideos.add(video);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            //Add new user
                            newVideo.put(APIConstants.VIDEO_LABEL_ID, UUID.randomUUID().toString());
                            newVideo.put(APIConstants.VIDEO_LABEL_TITLE, mVideoTitle.getText().toString());
                            newVideo.put(APIConstants.VIDEO_LABEL_DATE, new Date().toString());
                            newVideo.put(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL, mVideoThumbUrl.getText().toString());
                            newVideo.put(APIConstants.VIDEO_LABEL_TYPE, mVideoType);
                            newVideo.put(APIConstants.VIDEO_LABEL_USER_ID, UserSession.getInstance().getId());
                            newVideo.put(APIConstants.VIDEO_LABEL_TAGS, mVideoTags.getText().toString());

                            Log.d("Seeeeelecteeeed item ", mListVideoType.getSelectedItem().toString());

                            if(mListVideoType.getSelectedItem().toString().equals("Youtube")){
                                newVideo.put(APIConstants.VIDEO_LABEL_SOURCE, "Youtube");
                                getYoutubeVideoInfo((mVideoThumbUrl.getText().toString().length() > 0));
                            } else {
                                newVideo.put(APIConstants.VIDEO_LABEL_SOURCE, "Other");
                                newVideo.put(APIConstants.VIDEO_LABEL_URL, mVideoUrl.getText().toString());
                                newVideo.put(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL, R.drawable.wiideo_default_thumbnail + "");
                                listVideos.add(newVideo);

                                JSONArray jArrayVideos = APIHelper.buildVideoPost(listVideos);
                                requestAddVideo(jArrayVideos);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            mProgressDialog.dismiss();
                            if(e.getMessage() != null) Log.e(TAG, e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e(VolleyErrorHelper.getMessage(error, getContext()));
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), "User Register Error", Toast.LENGTH_SHORT).show();
                    }
                });

                addRequest.setTag(TAG); // to identify the request object
                AppController.getInstance().addToRequestQueue(addRequest);   // Access the RequestQueue through singleton class.

            } else {
                Toast.makeText(getContext(), "Please complete form", Toast.LENGTH_LONG).show();
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            mProgressDialog.dismiss();
            Toast.makeText(getContext(), "Unknown error", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * perform volley PUT request to add the new video
     * @param jArrayVideos list of the videos in jsonArrayFormat, use API Helper class
     */
    private void requestAddVideo(JSONArray jArrayVideos) {
        CustomJSONArrayRequest add = new CustomJSONArrayRequest(Request.Method.PUT, APIConstants.WII_LIBRARY,
                jArrayVideos /*new JSONArray(listVideos)*/, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                mProgressDialog.dismiss();

                Fragment home = new VideoGalleryFragment();
                FragmentTransaction transaction_register = getFragmentManager().beginTransaction();
                transaction_register.replace(R.id.home_fragment_container, home);
                transaction_register.addToBackStack(null);
                transaction_register.commit();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(VolleyErrorHelper.getMessage(error, getContext()));
                mProgressDialog.dismiss();
                Toast.makeText(getContext(), "User Register Error", Toast.LENGTH_SHORT).show();
            }
        });

        add.setTag(TAG); // to identify the request object
        AppController.getInstance().addToRequestQueue(add);
    }

    /**
     * getYoutubeVideoInfo
     * @param customThumbnail define if the user wants the youtube video thumbnail or his own
     */
    private void getYoutubeVideoInfo(boolean customThumbnail) {

        String videoId = "";
        if(!customThumbnail){
            String videoURL = mVideoUrl.getText().toString();
            int index = videoURL.indexOf("watch?v=");
            if(index != -1)
            {
                videoId = videoURL.substring(index + 8);
            }
        }

        newVideo.put(APIConstants.VIDEO_LABEL_CONTENT_ID, videoId);

        YouTubeVideoInfoRetriever youtubeVideoInfoRetriever = new YouTubeVideoInfoRetriever(mFragment, videoId);
        youtubeVideoInfoRetriever.setRetriever(this);
    }

    private boolean checkAddVideoForm() throws Exception {
        return mVideoTitle.getText().length() != 0
                && mVideoUrl.getText().length() != 0
                && mVideoTags.getText().length() != 0;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        mVideoType = parent.getItemAtPosition(pos).toString();
        switch (parent.getItemAtPosition(pos).toString()){
            case "Youtube":
                mVideoType = PlayerConstants.OTHER;
                break;
            case "DASH":
                mVideoType = PlayerConstants.DASH;
                break;
            case "SS":
                mVideoType = PlayerConstants.SS;
                break;
            case "HLS":
                mVideoType = PlayerConstants.HLS;
                break;
            case "Other":
                mVideoType = PlayerConstants.OTHER;
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    @Override
    public void retrieveYoutubeVideoThumbnail(String thumbUrl) {

        if(!thumbUrl.equals("")) newVideo.put(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL, thumbUrl);
        else newVideo.put(APIConstants.VIDEO_LABEL_VIDEO_THUMBNAIL, mVideoThumbUrl.getText().toString());

        //external library
        new YouTubeExtractor(getContext()) {
            @Override
            protected void onExtractionComplete(SparseArray<YtFile> sparseArray, VideoMeta videoMeta) {
                    try{
                        if (sparseArray != null) {
                            int itag = 22;
                            String downloadUrl = sparseArray.get(itag).getUrl();
                            if(downloadUrl != null) {
                                newVideo.put(APIConstants.VIDEO_LABEL_URL, downloadUrl);
                                listVideos.add(newVideo);

                                JSONArray jArrayVideos = APIHelper.buildVideoPost(listVideos);
                                requestAddVideo(jArrayVideos);

                                /* debug */
                                Log.d("YoutubeExtractor", downloadUrl);
                                for (int p = 0; p < sparseArray.size() ; p++ ) {
                                    if(sparseArray.get(p) != null) Log.d("ytfiles", p + " : " + sparseArray.get(p).toString());
                                }
                                /* end debug */
                            }else{
                                  throw new Exception(YOUTUBE_VIDEO_NOT_FOUND);
                            }
                        }else {
                                  throw new Exception(YOUTUBE_VIDEO_NOT_FOUND);
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), YOUTUBE_VIDEO_NOT_FOUND, Toast.LENGTH_SHORT).show();
                    }
            }
        }.extract(mVideoUrl.getText().toString(), true, true);
    }
}
