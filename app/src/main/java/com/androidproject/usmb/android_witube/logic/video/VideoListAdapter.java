package com.androidproject.usmb.android_witube.logic.video;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.model.video.Video;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class VideoListAdapter extends RecyclerView.Adapter<VideoItemViewHolder> implements Filterable {

    private Context mContext;

    private ArrayList<Video> videos;
    private ArrayList<Video> mFilteredList;

    private IVideoAdapterCallbackListener mIVideoAdapterCallbackListener;

    public static boolean isViewWithCatalog = true;

    public VideoListAdapter(Context context, Fragment fragment, ArrayList<Video> videos) {
        super();
        this.videos = videos;
        this.mFilteredList = videos;
        this.mContext = context;
        try {
            this.mIVideoAdapterCallbackListener = ((IVideoAdapterCallbackListener) fragment);
        } catch (ClassCastException e) {
            throw new ClassCastException("Fragment must implement IVideoAdapterCallbackListener.");
        }
    }

    @Override
    public VideoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(isViewWithCatalog ? R.layout.item_grid_video : R.layout.item_list_video, parent, false);
        return new VideoItemViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(VideoItemViewHolder holder, final int position) {

        if(mFilteredList.get(position).getVideoThumbnail().equals(R.drawable.wiideo_default_thumbnail + "")){
            Glide.with(mContext).load(R.drawable.wiideo_default_thumbnail)
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.mVideoThumbnail);
        }else{
            Glide.with(mContext).load(mFilteredList.get(position).getVideoThumbnail())
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.mVideoThumbnail);
        }
        holder.mVideoTitle.setText(mFilteredList.get(position).getTitle());

        holder.setClickListener(new IVideoAdapterClickListener() {
            @Override
            public void deleteVideo(int pos) {
                removeItem(pos);
            }

            @Override
            public void play(int pos) {
                try{
                    mIVideoAdapterCallbackListener.setToolbarIcons(true);
                    //mIVideoAdapterCallbackListener.openVideoView();
                    mIVideoAdapterCallbackListener.play(mFilteredList.get(pos));
                }catch (ClassCastException cce){
                    cce.printStackTrace();
                    mIVideoAdapterCallbackListener.exitPlayer();
                }
            }

        });
        //holder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() { return this.mFilteredList.size();}

    @SuppressWarnings("unused")
    public void addItem(Video video) {
        this.mFilteredList.add(video);
        this.videos.add(video);
        notifyItemInserted(mFilteredList.size());
    }

    public void removeItem(int position) {
        Video video = this.mFilteredList.get(position);
        this.mFilteredList.remove(position);
        this.videos.remove(video);
        mIVideoAdapterCallbackListener.exitPlayer();
        mIVideoAdapterCallbackListener.deleteVideo();
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, this.mFilteredList.size());
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    mFilteredList = videos;
                } else {

                    ArrayList<Video> filteredList = new ArrayList<>();

                    for (Video video : videos) {
                        Log.v("mot a chercher : ",charString);

                        for (String tag: video.getTags()) {
                            if (tag.toLowerCase().contains(charString)) {
                                Log.v("Video tag match :  ", tag);
                                filteredList.add(video);
                            }
                        }
                    }
                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //noinspection unchecked
                mFilteredList = (ArrayList<Video>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }
}
