package com.androidproject.usmb.android_witube.activities.entry;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidproject.usmb.android_witube.R;

/**
 * User Entry point of the application
 */
public class EntryFragment extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_entry, container, false);

        TextView button_connexion = (TextView) view.findViewById(R.id.btn_fe_sign_in_);
        button_connexion.setOnClickListener(this);

        Button button_email_register = (Button) view.findViewById(R.id.btn_fe_register);
        button_email_register.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case (R.id.btn_fe_sign_in_):
                Fragment signInFragment = new LoginFragment();
                FragmentTransaction transaction_signin = getFragmentManager().beginTransaction();
                transaction_signin.replace(R.id.ll_ar_root,signInFragment);
                transaction_signin.addToBackStack(null);
                transaction_signin.commit();
                break;

            case (R.id.btn_fe_register):
                Fragment signUpFragment = new RegisterFragment();
                FragmentTransaction transaction_signUpFragment = getFragmentManager().beginTransaction();
                transaction_signUpFragment.replace(R.id.ll_ar_root, signUpFragment);
                transaction_signUpFragment.addToBackStack(null);
                transaction_signUpFragment.commit();
                break;

        }
    }
}
