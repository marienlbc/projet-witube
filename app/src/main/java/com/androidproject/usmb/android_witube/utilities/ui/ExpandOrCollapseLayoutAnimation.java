package com.androidproject.usmb.android_witube.utilities.ui;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

/**
 *  Animation helper class
 */
public class ExpandOrCollapseLayoutAnimation {

    /**
     * Expand Layout to the desired size by using the screen height percentage
     * @param v layout to expand
     * @param screenHeight target height
     */
    public void expand(final View v, int screenHeight) {

        int prevHeight  = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, screenHeight); //take the preview height and the target height desired

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });

        valueAnimator.setInterpolator(new AccelerateInterpolator());
        valueAnimator.setDuration(500);
        valueAnimator.start();
    }

    /**
     * Collapse layout to the target height specified
     * @param v layout to expand
     * @param screenHeight target height
     */
    public void collapse(final View v, int screenHeight) {
        int prevHeight  = v.getHeight();
        int targetHeight = (int) (screenHeight * 0.02);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new AccelerateInterpolator());
        valueAnimator.setDuration(500);
        valueAnimator.start();
    }
}
