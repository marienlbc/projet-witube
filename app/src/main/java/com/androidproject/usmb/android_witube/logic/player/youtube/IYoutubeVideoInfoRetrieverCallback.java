package com.androidproject.usmb.android_witube.logic.player.youtube;

public interface IYoutubeVideoInfoRetrieverCallback {

    void retrieveYoutubeVideoThumbnail(String thumbUrl);
}
