package com.androidproject.usmb.android_witube.logic.api;

public class APIConstants {

    private static final String BASE_HOST_URL = "https://api.myjson.com/bins";

    public static final String WII_LIBRARY = BASE_HOST_URL + "/12tdyr";
    public static final String LOGIN_URL = BASE_HOST_URL + "/18ukmz";


    //region API Video Label

    public static final String VIDEO_LABEL_ID = "id";
    public static final String VIDEO_LABEL_TITLE = "title";
    public static final String VIDEO_LABEL_URL = "url";
    public static final String VIDEO_LABEL_DATE = "date";
    public static final String VIDEO_LABEL_COMMENT = "comment";
    public static final String VIDEO_LABEL_VIDEO_THUMBNAIL = "videoThumbnail";
    public static final String VIDEO_LABEL_PROVIDER = "provider";
    public static final String VIDEO_LABEL_CONTENT_ID = "contentId";
    public static final String VIDEO_LABEL_TYPE = "type";
    public static final String VIDEO_LABEL_USER_ID = "user_id";
    public static final String VIDEO_LABEL_TAGS = "tags";
    public static final String VIDEO_LABEL_SOURCE = "source";

    //endregion

    //region API User Label

    public static final String USER_LABEL_ID = "_id";
    public static final String USER_LABEL_USERNAME = "username";
    public static final String USER_LABEL_PASSWORD = "password";
    public static final String USER_LABEL_GRAVATAR_URL = "gravatar";
    public static final String USER_LABEL_EMAIL = "email";

    //endregion
}
