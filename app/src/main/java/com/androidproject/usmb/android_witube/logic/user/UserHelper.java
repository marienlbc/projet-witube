package com.androidproject.usmb.android_witube.logic.user;

import com.androidproject.usmb.android_witube.logic.api.APIConstants;
import com.androidproject.usmb.android_witube.model.user.User;
import com.androidproject.usmb.android_witube.model.user.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 *  User Helper Class
 */
public abstract class UserHelper {

    /**
     *  Convert User to json and then to String Format, used to save user session in shared preferences
     * @param userSession UserSession (User)
     * @return String
     * @throws JSONException json exception
     */
    public static String convertUserToString(UserSession userSession) throws JSONException {

        JSONObject jUserSession = new JSONObject();
        jUserSession.put(APIConstants.USER_LABEL_ID, userSession.getId());
        jUserSession.put(APIConstants.USER_LABEL_EMAIL, userSession.getEmail());
        jUserSession.put(APIConstants.USER_LABEL_USERNAME, userSession.getUsername());
        jUserSession.put(APIConstants.USER_LABEL_PASSWORD, userSession.getPassword());
        jUserSession.put(APIConstants.USER_LABEL_GRAVATAR_URL, userSession.getGravatarUrl());

        return jUserSession.toString();
    }

    public static User checkLogin(User requestLogin, ArrayList<User> usersSet){
        for (User u: usersSet) {if(u.equals(requestLogin)) return u;}
        return null;
    }

    public static String getGravatarUrl(String email) {
        String link = "http://www.gravatar.com/avatar/";
        String emailHashed;
        String defaultParam = "?d=identicon&s=48";
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(email.getBytes(),0,email.length());

            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            emailHashed = bigInt.toString(16);

            // Now we need to zero pad it if you actually want the full 32 chars.
            while(emailHashed.length() < 32 ){
                emailHashed = "0"+ emailHashed;
            }

            return link + emailHashed + defaultParam;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "https://www.gravatar.com/avatar/?d=retro";
        }

    }
}
