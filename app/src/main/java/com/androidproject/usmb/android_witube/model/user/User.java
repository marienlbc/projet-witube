package com.androidproject.usmb.android_witube.model.user;

/**
 *  User model class
 */
public class User {

    @SuppressWarnings("WeakerAccess")
    protected String _mId;
    @SuppressWarnings("WeakerAccess")
    protected String mEmail;
    @SuppressWarnings("WeakerAccess")
    protected String mUsername;
    @SuppressWarnings("WeakerAccess")
    protected String mPassword;
    @SuppressWarnings("WeakerAccess")
    protected String mGravatarUrl;

    public User() {
        this._mId = "";
        this.mUsername = "";
        this.mPassword = "";
        this.mEmail = "";
        this.mGravatarUrl = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return mUsername.equals(user.mUsername) && mPassword.equals(user.mPassword);
    }

    @Override
    public int hashCode() {
        int result = mUsername.hashCode();
        result = 31 * result + mPassword.hashCode();
        return result;
    }

    //region Accessors

    public String getId() { return _mId; }
    public void setId(String _id) { this._mId = _id; }

    public String getEmail() { return mEmail; }
    public void setEmail(String email) { this.mEmail = email; }

    public String getUsername() { return mUsername; }
    public void setUsername(String username) { this.mUsername = username; }

    public String getGravatarUrl() { return mGravatarUrl; }
    public void setGravatarUrl(String gravatarUrl) { this.mGravatarUrl = gravatarUrl; }

    public String getPassword() { return mPassword; }
    public void setPassword(String mPassword) { this.mPassword = mPassword; }

    //endregion



}
