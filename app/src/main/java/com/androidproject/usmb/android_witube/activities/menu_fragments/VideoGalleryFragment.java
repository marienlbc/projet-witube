package com.androidproject.usmb.android_witube.activities.menu_fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.androidproject.usmb.android_witube.AppController;
import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.logic.api.APIConstants;
import com.androidproject.usmb.android_witube.logic.api.APIHelper;
import com.androidproject.usmb.android_witube.logic.video.VideoListAdapter;
import com.androidproject.usmb.android_witube.model.user.UserSession;
import com.androidproject.usmb.android_witube.model.video.Video;
import com.androidproject.usmb.android_witube.logic.video.IVideoAdapterCallbackListener;
import com.androidproject.usmb.android_witube.utilities.ui.ImageUtility;
import com.androidproject.usmb.android_witube.utilities.volley.CustomJSONArrayRequest;
import com.androidproject.usmb.android_witube.utilities.volley.VolleyErrorHelper;
import com.androidproject.usmb.android_witube.utilities.volley.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Timestamp;
import java.util.ArrayList;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

/**
 * List of the videos Page
 */
public class VideoGalleryFragment extends Fragment implements IVideoAdapterCallbackListener, Response.Listener, Response.ErrorListener {

    private static final String TAG = "VolleyHelper"; //for debug

    private RecyclerView rvVideoList;
    private View dialog_view;
    private Menu menu;
    private TextView mNoVideo;

    private VideoListAdapter rvAdapter;

    private int nbGridColumns = 2;

    private ItemTouchHelper itemTouchHelper;
    private Paint p = new Paint();
    private PlayerView mPlayerView;

    public static ArrayList<Video> videos;

    public VideoGalleryFragment(){}

    @SuppressLint({"NewApi", "InflateParams"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_videos, container, false);
        setHasOptionsMenu(true);
        dialog_view = inflater.inflate(R.layout.dialog_add_note, null);
        mNoVideo = (TextView) view.findViewById(R.id.tv_flv_no_videos);

        if (savedInstanceState != null) {
            // Restore last state
            PlayerView.mIsPlayerViewOpen = savedInstanceState.getBoolean("player_active");
            this.setToolbarIcons(PlayerView.mIsPlayerViewOpen);
        }else {
            this.setToolbarIcons(false);
        }

        rvVideoList = (RecyclerView)view.findViewById(R.id.rv_flv_list_videos);
        rvVideoList.setHasFixedSize(true);

        if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            nbGridColumns = 3;
        }
        if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            nbGridColumns = 2;
        }
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), nbGridColumns);
        rvVideoList.setLayoutManager(layoutManager);

        videos = new ArrayList<>();
        rvAdapter = new VideoListAdapter(getContext(), this, videos);
        rvVideoList.setAdapter(rvAdapter);

        //rv swiping animation
        itemTouchHelper = new ItemTouchHelper(initSwipe());
        disableSwipe(); // as we start with a grid layout

        mPlayerView = new PlayerView(getActivity(),getContext(), view);

        //check if there is valid data in the cache
        String responseFromCache = VolleyHelper.getDataFromCache(APIConstants.WII_LIBRARY, true);
        if(!responseFromCache.equals(VolleyHelper.NO_CACHE_DATA) && !responseFromCache.equals(VolleyHelper.CACHE_ERROR) && !responseFromCache.equals(VolleyHelper.CACHE_EXPIRED)){
            try {
                VolleyLog.d(responseFromCache);
                videos.addAll(APIHelper.parseGetVideosResponse(new JSONArray(responseFromCache)));
                rvAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(),VolleyHelper.CACHE_ERROR, Toast.LENGTH_LONG).show();
                getVideos();
            }
        }
        else{
            getVideos();
        }

        if(videos.isEmpty()){
            mNoVideo.setVisibility(View.VISIBLE);
            mNoVideo.setText("Hello " + UserSession.getInstance().getUsername() + " ! \n You don't have any current videos \n" +
                    " Please Add one from the topbar or through the left menu");
        }

        return view;
    }

    /**
     *  get videos API request
     */
    private void getVideos(){
        CustomJSONArrayRequest jsonRequest = new CustomJSONArrayRequest(Request.Method.GET, APIConstants.WII_LIBRARY, this, this);
        jsonRequest.setTag(TAG); // to identify the request object
        // Access the RequestQueue through singleton class.
        AppController.getInstance().addToRequestQueue(jsonRequest);
    }

    public boolean isYoutubeLinkExpired(String youtubeLink) throws Exception{
        String linkItems[] = youtubeLink.split("&");
        for (String linkItem: linkItems) {
            if(linkItem.startsWith("expire")){
                int index = linkItem.indexOf("expire=");
                if(index != -1)
                {
                    if(Long.valueOf(linkItem.substring(7)) < (System.currentTimeMillis() / 1000)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //region Toolbar

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        menuInflater.inflate(R.menu.main_menu, this.menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //this.setToolbarIcons(PlayerView.mIsPlayerViewOpen);

        if( id == R.id.action_close_video) {
            if(mPlayerView.closePlayerView()){
                this.setToolbarIcons(false);
                rvVideoList.setEnabled(true);
            }
        }

        if( id == R.id.action_add_video_note) {
            mPlayerView.pause();
            AlertDialog.Builder addVideoAlertDialog = new AlertDialog.Builder(getContext());
            removeView();
            addVideoAlertDialog.setView(dialog_view);
            final EditText time = (EditText) dialog_view.findViewById(R.id.et_and_current_time);
            TextView current_note = (TextView) dialog_view.findViewById(R.id.tv_and_currentComment);
            if(!mPlayerView.getVideoInstance().getComment().equals("")){
                String comment = mPlayerView.getVideoInstance().getComment();
                current_note.setText(comment.substring(0, 8) + " " + comment.substring(8, comment.length()));
            }
            time.setText(mPlayerView.getCurrentPlayingTime());
            addVideoAlertDialog.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Logic add here
                    EditText note = (EditText) dialog_view.findViewById(R.id.et_and_note);
                    addVideoNote(time.getText().toString(), note.getText().toString());
                    dialog.dismiss();
                    mPlayerView.resume();
                }
            });
            addVideoAlertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mPlayerView.resume();
                }
            });
            addVideoAlertDialog.show();
            //logic
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search_video) {
            Toast.makeText(getActivity().getApplicationContext(), "Search Video Functionality", Toast.LENGTH_LONG).show();

            SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
            search(searchView);
        }
        if(id == R.id.action_switch_view){
            VideoListAdapter.isViewWithCatalog = !VideoListAdapter.isViewWithCatalog;
            item.setIcon(getResources().getDrawable((VideoListAdapter.isViewWithCatalog) ? R.drawable.ic_view_list_black_24px : R.drawable.ic_view_column_black_24px));

            rvVideoList.setLayoutManager(VideoListAdapter.isViewWithCatalog ? new GridLayoutManager(getContext(), nbGridColumns) : new LinearLayoutManager(getContext()));
            rvVideoList.setAdapter(rvAdapter);

            if(VideoListAdapter.isViewWithCatalog){
                disableSwipe();
            }else{
                enableSwipe();
            }
        }
        if(id == R.id.action_add_video){
            Fragment addVideoFragment = new AddVideoFragment();
            FragmentTransaction transaction_register = getFragmentManager().beginTransaction();
            transaction_register.replace(R.id.home_fragment_container, addVideoFragment);
            transaction_register.commit();
        }

        if(id == R.id.action_delete_video){
            int pos = videos.indexOf(mPlayerView.getVideoInstance());
            rvAdapter.removeItem(pos);
            this.setToolbarIcons(false);
            mNoVideo.setVisibility(View.VISIBLE);
            mNoVideo.setText("Hello " + UserSession.getInstance().getUsername() + " ! \n You don't have any current videos \n" +
                    " Please Add one from the topbar or through the left menu");

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolbarIcons(boolean onPlayingVideo) {
        if(this.menu != null){
            if(onPlayingVideo){
                this.menu.setGroupVisible(R.id.main_menu_group, false);
                this.menu.setGroupVisible(R.id.on_playing_video, true);
            }
            else {
                this.menu.setGroupVisible(R.id.on_playing_video, false);
                this.menu.setGroupVisible(R.id.main_menu_group, true);
            }
        }
    }

    //endregion

    //region Video Player

    @Override
    public void play(Video video) {
        rvVideoList.setEnabled(false);
        mPlayerView.play(video, videos.indexOf(video));
    }

    @Override
    public void exitPlayer() {
        rvVideoList.setEnabled(true);
        mPlayerView.exitPlayer();
    }

    public void addVideoNote(String time, String note) {
        for (Video video: videos  ) {
            if(video.getId().equals(mPlayerView.getVideoInstance().getId())){
                video.setComment(time + " " + note);
            }
        }

        JSONArray jArrayVideos = APIHelper.buildVideoFromVideo(videos);

        CustomJSONArrayRequest addVideoNote = new CustomJSONArrayRequest(Request.Method.PUT, APIConstants.WII_LIBRARY,
                jArrayVideos /*new JSONArray(listVideos)*/, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Toast.makeText(getContext(), "Note added", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(VolleyErrorHelper.getMessage(error, getContext()));
                Toast.makeText(getContext(), "Add note error", Toast.LENGTH_SHORT).show();
            }
        });

        addVideoNote.setTag(TAG); // to identify the request object
        AppController.getInstance().addToRequestQueue(addVideoNote);
    }

    private void removeView(){
        if(dialog_view.getParent()!=null) {
            ((ViewGroup) dialog_view.getParent()).removeView(dialog_view);
        }
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                rvAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    private void refreshYoutubeLink(String videoID, final int videoIndex) throws Exception {

        String youtubelink = "https://www.youtube.com/watch?v=" + videoID;
        //Toast.makeText(getContext(), youtubelink, Toast.LENGTH_SHORT).show();
        new YouTubeExtractor(getContext()) {
            @Override
            protected void onExtractionComplete(SparseArray<YtFile> sparseArray, VideoMeta videoMeta) {
                if (sparseArray != null) {
                    int itag = 22;
                    try{
                        String downloadUrl = sparseArray.get(itag).getUrl();
                        if(downloadUrl != null) {
                            videos.get(videoIndex).setUrl(downloadUrl);
                            Toast.makeText(getContext(), "Youtube links refreshed", Toast.LENGTH_SHORT).show();
                            rvAdapter.notifyDataSetChanged();
                        }else{
                            Toast.makeText(getActivity(),"Could not refresh Youtube links", Toast.LENGTH_SHORT).show();
                            throw new Exception();
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.extract(youtubelink, true, true);
    }

    //endregion

    //region SwapViewHandling

    private ItemTouchHelper.SimpleCallback initSwipe(){
        return new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT){
                    rvAdapter.removeItem(position);
                } else {
                    rvAdapter.removeItem(position);
                }
            }
            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dX > 0){
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = ImageUtility.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_delete_black_24px);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = ImageUtility.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_delete_black_24px);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
    }

    private void enableSwipe(){
        itemTouchHelper.attachToRecyclerView(rvVideoList);
    }

    private void disableSwipe(){
        itemTouchHelper.attachToRecyclerView(null);
    }

    //endregion

    //region Listeners

    @Override
    public void onErrorResponse(VolleyError error) {

        Toast.makeText(getActivity(), VolleyErrorHelper.getMessage(error,getContext()), Toast.LENGTH_SHORT).show();


        //try to retrieve cache if error with volley
        String responseFromCache = VolleyHelper.getDataFromCache(APIConstants.WII_LIBRARY, false);
        if(!responseFromCache.equals(VolleyHelper.NO_CACHE_DATA) && !responseFromCache.equals(VolleyHelper.CACHE_ERROR)){
            try {
                videos.clear();
                videos.addAll(APIHelper.parseGetVideosResponse(new JSONArray(responseFromCache)));
                rvAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(),VolleyHelper.CACHE_ERROR, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResponse(Object response) {
        VolleyLog.d(TAG, "Volley Response: " + response.toString());

        videos.clear();

        try{
            videos.addAll(APIHelper.parseGetVideosResponse((JSONArray) response));

            for (int i=0; i<videos.size(); i++) {
                if(videos.get(i).getSource().equals("Youtube")){

                    try{
                        if(isYoutubeLinkExpired(videos.get(i).getUrl())){
                            refreshYoutubeLink(videos.get(i).getContentId(), i);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            if(videos.isEmpty()){
                mNoVideo.setVisibility(View.VISIBLE);
                mNoVideo.setText("Hello " + UserSession.getInstance().getUsername() + " ! \n You don't have any current videos \n" +
                        " Please Add one from the topbar or through the left menu");
            }else {
                mNoVideo.setVisibility(View.GONE);
            }

            rvAdapter.notifyDataSetChanged();
            //if(refresh.length > 0) refreshYoutubeLink(refresh);

        } catch (JSONException e) {
            e.printStackTrace();
            //Toast.makeText(getActivity(),VolleyHelper.CACHE_ERROR, Toast.LENGTH_SHORT).show();
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getActivity(),"Could not refresh Youtube links", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void deleteVideo(){

        JSONArray jArrayVideos = APIHelper.buildVideoFromVideo(videos);

        CustomJSONArrayRequest add = new CustomJSONArrayRequest(Request.Method.PUT, APIConstants.WII_LIBRARY,
                jArrayVideos /*new JSONArray(listVideos)*/, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Toast.makeText(getContext(), "Video deleted", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(VolleyErrorHelper.getMessage(error, getContext()));
                Toast.makeText(getContext(), "Video could not be deleted", Toast.LENGTH_SHORT).show();
            }
        });

        add.setTag(TAG); // to identify the request object
        AppController.getInstance().addToRequestQueue(add);
    }

    /**
     * screen rotation
     * @param outState bundle state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("player_active",  PlayerView.mIsPlayerViewOpen);
    }

    //endregion
}