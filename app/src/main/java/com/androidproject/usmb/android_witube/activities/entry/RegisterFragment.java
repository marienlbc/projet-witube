package com.androidproject.usmb.android_witube.activities.entry;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.androidproject.usmb.android_witube.AppController;
import com.androidproject.usmb.android_witube.R;
import com.androidproject.usmb.android_witube.activities.MainActivity;
import com.androidproject.usmb.android_witube.logic.api.APIConstants;
import com.androidproject.usmb.android_witube.logic.user.UserHelper;
import com.androidproject.usmb.android_witube.model.user.User;
import com.androidproject.usmb.android_witube.model.user.UserSession;
import com.androidproject.usmb.android_witube.utilities.storage.SharedPreferenceHelper;
import com.androidproject.usmb.android_witube.utilities.volley.CustomJSONArrayRequest;
import com.androidproject.usmb.android_witube.utilities.volley.VolleyErrorHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *  User Register Page
 */
public class RegisterFragment extends Fragment implements View.OnClickListener{

    private static final String TAG = RegisterFragment.class.getSimpleName();

    public static final String NON_MATCH_PASSWORD = "non-match-password";

    private EditText mEditTextEmail;
    private EditText mEditTextUsername;
    private EditText mEditTextPassword;
    private EditText mEditTextConfirmPassword;
    private ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        mEditTextEmail = (EditText) view.findViewById(R.id.et_fsu_email);
        mEditTextUsername = (EditText) view.findViewById(R.id.et_fsu_username);
        mEditTextPassword = (EditText) view.findViewById(R.id.et_fsu_password);
        mEditTextConfirmPassword = (EditText) view.findViewById(R.id.et_fsu_confirm_password);
        Button mRegister = (Button) view.findViewById(R.id.btn_fsu_register);
        TextView mTxtSignIn = (TextView) view.findViewById(R.id.txt_fsu_sign_in);

        mRegister.setOnClickListener(this);
        mTxtSignIn.setOnClickListener(this);

        mProgressDialog = new ProgressDialog(getActivity());

        return view;
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.btn_fsu_register :
                register();
                break;
            case R.id.txt_fsu_sign_in :
                Fragment signInFragment = new LoginFragment();
                FragmentTransaction transaction_signInFragment = getFragmentManager().beginTransaction();
                transaction_signInFragment.replace(R.id.ll_ar_root, signInFragment);
                transaction_signInFragment.addToBackStack(null);
                transaction_signInFragment.commit();
                break;
            default:
                break;
        }

    }

    private void register() {

        mProgressDialog.setMessage("Connecting....");
        mProgressDialog.show();

        try {
            if(checkSignUpForm()){

                // really bad way to do this but we don't have our own API server so we can't perform basic API operations
                //1 get all users
                CustomJSONArrayRequest loginRequest  = new CustomJSONArrayRequest(Request.Method.GET, APIConstants.LOGIN_URL, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            List<Map<String,String>> listUsers =  new ArrayList<>();
                            final User u = new User();

                            for (int i=0; i<response.length(); i++) {
                                try {
                                    Map<String,String> user  = new HashMap<String,String>();

                                    JSONObject juser = (JSONObject) response.get(i);

                                    user.put(APIConstants.USER_LABEL_ID, juser.getString(APIConstants.USER_LABEL_ID));
                                    user.put(APIConstants.USER_LABEL_EMAIL, juser.getString(APIConstants.USER_LABEL_EMAIL));
                                    user.put(APIConstants.USER_LABEL_USERNAME, juser.getString(APIConstants.USER_LABEL_USERNAME));
                                    user.put(APIConstants.USER_LABEL_PASSWORD, juser.getString(APIConstants.USER_LABEL_PASSWORD));
                                    user.put(APIConstants.USER_LABEL_GRAVATAR_URL, juser.getString(APIConstants.USER_LABEL_GRAVATAR_URL));

                                    listUsers.add(user);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            //Add new user
                            final Map<String, String> newUser = new HashMap<>();
                            newUser.put(APIConstants.USER_LABEL_ID, UUID.randomUUID().toString());
                            newUser.put(APIConstants.USER_LABEL_USERNAME, mEditTextUsername.getText().toString());
                            newUser.put(APIConstants.USER_LABEL_PASSWORD, mEditTextPassword.getText().toString());
                            newUser.put(APIConstants.USER_LABEL_EMAIL, mEditTextEmail.getText().toString());
                            newUser.put(APIConstants.USER_LABEL_GRAVATAR_URL, UserHelper.getGravatarUrl(mEditTextEmail.getText().toString()));

                            listUsers.add(newUser);
                            u.setId(newUser.get(APIConstants.USER_LABEL_ID));
                            u.setUsername(newUser.get(APIConstants.USER_LABEL_USERNAME));
                            u.setPassword(newUser.get(APIConstants.USER_LABEL_PASSWORD));
                            u.setEmail(newUser.get(APIConstants.USER_LABEL_EMAIL));
                            u.setGravatarUrl(newUser.get(APIConstants.USER_LABEL_GRAVATAR_URL));

                            CustomJSONArrayRequest register = new CustomJSONArrayRequest(Request.Method.PUT, APIConstants.LOGIN_URL,
                                    new JSONArray(listUsers), new Response.Listener<JSONArray>() {
                                @Override
                                public void onResponse(JSONArray response) {

                                    //set user session (singleton)
                                    UserSession userSession = UserSession.getInstance();
                                    userSession.setId(u.getId());
                                    userSession.setUsername(u.getUsername());
                                    userSession.setPassword(u.getPassword());
                                    userSession.setGravatarUrl(u.getGravatarUrl());
                                    userSession.setEmail(u.getEmail());

                                    //update user session
                                    try {
                                        SharedPreferenceHelper.setUserSession(getContext(), UserHelper.convertUserToString(userSession));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    mProgressDialog.dismiss();

                                    Intent home = new Intent(getContext(), MainActivity.class);
                                    startActivity(home);

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.e(VolleyErrorHelper.getMessage(error, getContext()));
                                    mProgressDialog.dismiss();
                                    Toast.makeText(getContext(), "User Register Error", Toast.LENGTH_SHORT).show();
                                }
                            });

                            register.setTag(TAG); // to identify the request object
                            AppController.getInstance().addToRequestQueue(register);

                        } catch (Exception e) {
                            e.printStackTrace();
                            mProgressDialog.dismiss();
                            Log.e(TAG, e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e(VolleyErrorHelper.getMessage(error, getContext()));
                        mProgressDialog.dismiss();
                        Toast.makeText(getContext(), "User Register Error", Toast.LENGTH_SHORT).show();
                    }
                });

                loginRequest.setTag(TAG); // to identify the request object
                AppController.getInstance().addToRequestQueue(loginRequest);   // Access the RequestQueue through singleton class.

            }else {
                Toast.makeText(getContext(), "Please complete form", Toast.LENGTH_LONG).show();
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            mProgressDialog.dismiss();
            if(e.getMessage().equals(NON_MATCH_PASSWORD)){
                Toast.makeText(getContext(), "Passwords does not match", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getContext(), "Unknown error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean checkSignUpForm() throws Exception {

        if(mEditTextEmail.getText().length() == 0) return false;
        if(mEditTextUsername.getText().length() == 0) return false;
        if(mEditTextPassword.getText().length() == 0) return false;
        if(!mEditTextConfirmPassword.getText().toString().equals(mEditTextPassword.getText().toString())){
            throw new Exception(NON_MATCH_PASSWORD);
        }
        return true;
    }
}
