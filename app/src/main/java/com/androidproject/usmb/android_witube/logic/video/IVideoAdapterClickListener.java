package com.androidproject.usmb.android_witube.logic.video;

@SuppressWarnings("WeakerAccess")
public interface IVideoAdapterClickListener {

    void play(int pos);

    void deleteVideo(int pos);

}
