package com.androidproject.usmb.android_witube.utilities.volley;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;


public class CustomJSONArrayRequest extends JsonArrayRequest{

    public CustomJSONArrayRequest(int method, String url, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        super(method, url, null, listener, errorListener);
    }

    public CustomJSONArrayRequest(int method, String url, JSONArray jsonArray, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonArray, listener, errorListener);
    }
}